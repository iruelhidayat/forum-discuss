## Final Project

## Kelompok 15

## Anggota Kelompok

-   Nurul Hidayat (@iruelhidayat)
-   Muhamad Yoga Subarkah (@yogaaof)
-   Ragil Andika Johan (@Ragiljohan)

## Tema Project

Forum Discuss

## ERD

<p align="center"><img src="public/images/ERD_Forum.png"></p>

## Link Video

-   Link Video Aplikasi : [https://www.youtube.com/watch?v=p0HcvFRD_eg](https://www.youtube.com/watch?v=p0HcvFRD_eg).
-   Link Deploy : [https://forum.sanbercodeapp.com/](https://forum.sanbercodeapp.com/).

## Template

-   landingpage: [https://themewagon.com/themes/dtox-free-bootstrap-4-html5-landing-page-template/](https://themewagon.com/themes/dtox-free-bootstrap-4-html5-landing-page-template/).
-   Dashboard: [https://themewagon.com/themes/free-bootstrap-4-html-5-admin-dashboard-website-template-skydash/](https://themewagon.com/themes/free-bootstrap-4-html-5-admin-dashboard-website-template-skydash/).
